<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome')->name('home');
});

Route::get('/register' , 'RegController@open')->name('register');
Route::post('/done' , 'RegController@send')->name('donreg');




Route::get('/do' , 'RegController@getall');




Route::get('/login' , 'RegController@login')->name('login');
Route::post('/dolog' , 'RegController@dolog')->name('donlog');




Route::post('/logout', 'RegController@logout')->name('logout');


Route::post('/post', 'RegController@post')->name('post');

Route::get('/getpost', 'PostController@post')->name('getpost');






